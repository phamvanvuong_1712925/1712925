
async function eventSubmit(e){
    e.preventDefault();
    pages_movieSearch(1);
}

function loading() {
    $('#main').empty();
    $('#main2').empty();
    $('#main3').empty();
    $('#main4').empty();
    $('#main5').empty();
    $('#bottom').empty();
    $('#main').append(`<div class="spinner-grow" role="status">
    <span class="sr-only">Loading...</span>
    </div>`);
}

function fillMovies(ms){
    $('#main').empty();
    
    for(const m of ms){
        $('#main').append(`
        <div class="col-md-4 py-1">
                <div class="card shadow h-100">
                    <img src="https://image.tmdb.org/t/p/w500/${m.poster_path}" class="card-img-top alt="${m.title}"
                    <div class="card-body">
                    <h5 class="card-title">${m.title}</h5>
                    <p class="card-text">Rate: ${m.popularity}</p>
                    <p class="card-text">Vote: ${m.vote_average}</p>
                    <p class="card-text">Release Date: ${m.release_date}</p>
                    <a href="#" class="btn btn-primary" onclick= loadDetail(${m.id})>Xem chi tiết</a>
                </div>
        </div>`);
    }
}
async function pages_movieSearch(pages){
    const strSearch=$('form input').val();
    const reqStr=`https://api.themoviedb.org/3/search/multi?api_key=a3089cab612a318ef50654d165298381&language=en-US&query=${strSearch}&page=${pages}&&per_page=1`;

    loading();
    const response=await fetch(reqStr);
    const rs=await response.json();
    console.log(rs);
    for(const m of rs.results){
        if(m.media_type==="movie"){
            $('#main').append(`
        <div class="col-md-4 py-1">
                <div class="card shadow h-100">
                    <img src="https://image.tmdb.org/t/p/w500/${m.poster_path}" class="card-img-top alt="${m.title}"
                    <div class="card-body">
                    <h5 class="card-title">${m.title}</h5>
                    <p class="card-text">Rate: ${m.popularity}</p>
                    <p class="card-text">Vote: ${m.vote_average}</p>
                    <p class="card-text">Release Date: ${m.release_date}</p>
                    <a href="#" class="btn btn-primary" onclick= loadDetail(${m.id})>Xem chi tiết</a>
                </div>
        </div>`);
        }
        if(m.media_type==="person"){
            for(const n of m.known_for){
                $('#main').append(`
                <div class="col-md-4 py-1">
                        <div class="card shadow h-100">
                            <img src="https://image.tmdb.org/t/p/w500/${n.poster_path}" class="card-img-top alt="${n.title}"
                            <div class="card-body">
                            <h5 class="card-title">${n.title}</h5>
                            <p class="card-text">Rate: ${n.popularity}</p>
                            <p class="card-text">Vote: ${n.vote_average}</p>
                            <p class="card-text">Release Date: ${n.release_date}</p>
                            <a href="#" class="btn btn-primary" onclick= loadDetail(${n.id})>Xem chi tiết</a>
                        </div>
                </div>`);
            }
        }
    }
  pagination();
  $('.pagination li:not(:last-child):not(:first-child)').remove();
            for (let i = 1; i <= rs.total_pages; i++) {
                $('.pagination li:last-child').before(`<li class="page-item ${i === pages ? "active" : ""}" >
                                                    <a class="page-link" href="#" onclick=pages_movieSearch(${i})>${i}</a>
                                                   </li>`)
             }
}

function pagination(){
    $('#page').empty();
    $('#page').append(`<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>`);
}

async function loadDetail(id){
    const reqStr2=`https://api.themoviedb.org/3/movie/${id}?api_key=a3089cab612a318ef50654d165298381&language=en-US`;
    
    loading();
    const response2=await fetch(reqStr2);
    const movie=await response2.json();
    console.log(movie);

    const res = await fetch(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=a3089cab612a318ef50654d165298381`);
    const Cha_Dir= await res.json();
    console.log(Cha_Dir);

    
    fillMovie(movie);

    $('#main4').append(`
    <div class="col-md-1" style="color: black; float: left">
    <p class="card-text">Character: </p></div>`);
    
     for(const temp of Cha_Dir.cast){
            $('#main4').append(`<div class="col-md-1" style="color: red; float: left">
            <p class="btn btn-link" onclick=actorDetail(${temp.id})>${temp.name}</p>
            </div>`);
    }

    $('#main3').append(`<div class="col-md-1" style="color: black; float: left">
    <p class="card-text">Director: </p>
               </div>`);
    for(const p of Cha_Dir.crew){
        if(p.department==="Directing"){
          $('#main3').append(`<div class="col-md-1" style="color: red; float: left">
    <p class="card-text">${p.name}</p>
               </div>`);
        }
    }


    const res2 = await fetch(`https://api.themoviedb.org/3/movie/${id}/reviews?api_key=a3089cab612a318ef50654d165298381`);
    const rev= await res2.json();
    console.log(rev);
    $('#bottom').empty();
    $('#main5').append(`<div class="card">
        <div class="card-body" style="background-color: darkgrey;">
          REVIEW
        </div>
    </div>`);
    for(const temp of rev.results){
        $('#bottom').append(`<div class="card">
        <div class="card-body">
          ${temp.author}:  ${temp.content}
        </div>
      </div>`);
    }
    $('#page').empty();
}

function fillMovie(m){
    $('#main').empty();
    $('#main').append(`<div class="card-header">
        <img src="https://image.tmdb.org/t/p/w500/${m.poster_path}" class="card-img-top alt="${m.original_title}"
        </div>`);
    $('#main').append( `<div class="card-body">
        <h5 class="card-title">${m.original_title}</h5>
        <p class="card-text">Overview: ${m.overview}</p>
        <p class="card-text">Release Date: ${m.release_date}</p>
        </div>`);
    $('#main2').append(`<div class="col-md-1" style="color: black">
        <p class="card-text">Genres: </p>
        </div>`);
   for(const temp of m.genres){
        $('#main2').append(`<div class="col-md-1" style="color: red">
        <p class="card-text">${temp.name}</p>
        </div>`);
  }
}

async function actorDetail(id_actor){

    loading();
    const response4 = await fetch(`https://api.themoviedb.org/3/person/${id_actor}?api_key=a3089cab612a318ef50654d165298381&language=en-US`);
    const dt_actor = await response4.json();

    const response5= await fetch(`https://api.themoviedb.org/3/person/${id_actor}/movie_credits?api_key=a3089cab612a318ef50654d165298381&language=en-US`)
    const list_movie = await response5.json();
    
    $('#main').empty();
    $('#main').append(`<div class="col-md-6 py-1">
    <div class="card shadow h-100">
        <img src="https://image.tmdb.org/t/p/w500/${dt_actor.profile_path}" class="card-img-top alt="${dt_actor.name}"
        <div class="card-body">
            <h5 class="card-title">${dt_actor.name}</h5>
            <p class="card-text">Birthday:  ${dt_actor.birthday}</p>
            <p class="card-text">Place Of Birth:  ${dt_actor.place_of_birth}</p>
        </div>
    </div>`);

    $('#main2').append(`<div class="col-md-1" style="color: black; float: left">
        <p class="card-text">Movie: </p>
        </div>`);
    for(const temp of list_movie.cast){
        $('#main2').append(`<div class="col-md-1" style="color: red; float: left">
        <p class="btn btn-link" onclick="loadDetail(${temp.id})">${temp.original_title}</p>
        </div>`);

    }


}
async function loadhome(pages){
  loading();
  $('#page').empty();
  //const response3 = await fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=a3089cab612a318ef50654d165298381&language=en-US&page=1`);
  const response3 = await fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=a3089cab612a318ef50654d165298381&language=en-US&page=${pages}&&per_page=2`);
  const users = await response3.json();
  console.log(users);
  fillMovies(users.results);
  $('#page').append(`<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>`);
  $('.pagination li:not(:last-child):not(:first-child)').remove();
            for (let i = 1; i <= users.total_pages; i++) {
                $('.pagination li:last-child').before(`<li class="page-item ${i === pages ? "active" : ""}" >
                                                    <a class="page-link" href="#" onclick=loadhome(${i})>${i}</a>
                                                   </li>`)
             }
}